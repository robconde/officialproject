import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import Swal from 'sweetalert2';
declare const $: any;

export interface registerForm{
  username: string;
  email: string;
  password: string;
  state: string;
  user_type: string;
  

}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent {

  registerForm: registerForm ={
    username: '',
    email: '',
    password:'',
    state: 'active',
    user_type: '',
    
  }
  constructor(private router: Router, private authentication: AuthenticationService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    
    const {username, email, password, state, user_type} = this.registerForm;
    
    this.authentication.register(username, email, password, state, user_type)
    .subscribe( ok =>{
      if(ok==='true'){
        this.router.navigateByUrl('/reports')
      }else{
        Swal.fire('Error', ok, 'error')
      }
    })
   
  
}

}
