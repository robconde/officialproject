import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import Swal from 'sweetalert2';

interface loginForm{
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})



export class LoginComponent implements OnInit {

  loginForm: loginForm = {
    email: '',
    password: ''
  }

  constructor(private router: Router, private authentication: AuthenticationService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    
      const {email, password} = this.loginForm;
      
      this.authentication.login(email, password)
      .subscribe( ok =>{

        if(ok===true){
          this.router.navigateByUrl('/reports')
        }else{
          Swal.fire('Error', ok, 'error')
        }
      })
     
    
  }



}
