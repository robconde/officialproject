export interface authenticationResponse {
    ok: boolean;
    username?: string;
    email?:    string;
    state?: string;
    user_type?: string;
    token?:    string;
    msg?:      string;
}


export interface Usuario{
    username: string;
    email: string;
    state: string;
    user_type: string;
}