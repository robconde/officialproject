import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationRoutes } from './authentication.routing';
import { MaterialModule } from '../app.module';
import { FormsModule } from '@angular/forms';




@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  exports:[
    LoginComponent,
    RegisterComponent
    
  ], 
  imports: [
    CommonModule,
    RouterModule.forChild(AuthenticationRoutes),
    MaterialModule,
    FormsModule,
    RouterModule
  ]
})
export class AuthenticationModule { }
