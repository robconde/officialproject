import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthenticationRoutes } from '../authentication.routing';
import { authenticationResponse, Usuario } from '../interface/authentication.interface';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseUrl: string = 'http://localhost:5000';
  private _usuario!: Usuario;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
      
    })
  };

  get usuario(){
    return { ...this._usuario };
  }

  constructor(private http: HttpClient) { }


  login(email: string, password: string){
    
    const loginData = {email, password}
    const url = `${this.baseUrl}/auth/login`
     return this.http.post<authenticationResponse>(url, loginData, this.httpOptions)
     .pipe(
       tap(resp=>{

          if(resp.ok){
            localStorage.setItem('token', resp.token!);
            this._usuario = {
              username: resp.username,
              email: resp.email,
              state: resp.state,
              user_type: resp.user_type
            }
          }
       }),
       map(resp => resp.ok),
       catchError(err => of(err.error.msg))
     )
  }

  validarToken():Observable<boolean>{
    const url = `${this.baseUrl}/auth/checkToken`;
    const headers = new HttpHeaders()
    .set('x-token', localStorage.getItem('token') || '')
    
    return this.http.get<authenticationResponse>(url, {headers} )
      .pipe(
        map(resp =>{
          localStorage.setItem('token', resp.token!);
          this._usuario = {
             username: resp.username!,
             email: resp.email!,
             state: resp.state!,
             user_type: resp.user_type!
          }
          
          return resp.ok
        }),
        catchError(err => of(false))
      )

  }


  logOut(){
    localStorage.removeItem('token');

  }

  // register(username: string, email: string, password: string, state: string, user_type: string){
    
  //   const registerData = {username, email, password, state, user_type}
  //   const url = `${this.baseUrl}/auth/register`
  //    return this.http.post<authenticationResponse>(url, registerData, this.httpOptions)
  //    .pipe(
  //      tap(resp=>{
  //       console.log(resp)
  //         if(resp.ok){
  //           localStorage.setItem('token', resp.token!);
  //           this._usuario = {
  //             username: resp.username,
  //             email: resp.email,
  //             state: resp.state,
  //             user_type: resp.user_type
  //           }
  //         }
  //      }),
  //      map(resp => resp.ok),
  //      catchError(err => of(err.error.msg))
  //    )
  // }


  register(username: string, email: string, password: string, state: string, user_type: string){
    
    const registerData = {username,email, password, state, user_type}
    const url = `${this.baseUrl}/auth/register`
     return this.http.post<authenticationResponse>(url, registerData, this.httpOptions)
     .pipe(
       tap(resp=>{

          if(resp.ok){
            localStorage.setItem('token', resp.token!);
            this._usuario = {
              username: resp.username,
              email: resp.email,
              state: resp.state,
              user_type: resp.user_type
            }
          }
       }),
       map(resp => resp.ok),
       catchError(err => of(err.error.msg))
     )
  }
}
