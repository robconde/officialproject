import { Routes } from '@angular/router';
import { ReportsComponent } from './reports/reports.component';




export const ReportsRoute: Routes = [
    {
      path: '',
      children: [ {
        path: '',
        component: ReportsComponent
    }]}
];
