import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ShowGridResponse, Result } from '../interface/reports.interface'


@Injectable({
  providedIn: 'root'
})


export class ReportsService {

  //TODO cambiar any por su tipo correspondiente
  public resultados : Result[] = [];

  constructor(private http: HttpClient) {

   }

   
   urlBase = 'http://localhost:5000/grid';


    //params = new HttpParams().set('header', );


   private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
      
    })
  };

   sendFunction(funcion, userId){
     
     const body = {funcion, userId};
     console.log(body)
      return this.http.post<ShowGridResponse>(`${this.urlBase}/getSelect`, body, this.httpOptions);
   }
}
