import { Component, OnInit } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ReportsService } from '../services/reports.service';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication/service/authentication.service';
import { Router } from '@angular/router';


interface Formulario{
  funcion: string;
  userId: number;
}

declare const $: any;

@Component({
  selector: 'reports',
  templateUrl: './reports.component.html'
})
export class ReportsComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;

  formulario: Formulario = {
    funcion: '',
    userId: 1
  }

  get resultados() {
    return this.reportsService.resultados;
  }

  defaultColDef = {
    sortable: true,
    filter: true,
    editable: true
  }
  columnDefs = [
    { field: 'id', checkboxSelection: true },
    { field: 'funcion'  },
    { field: 'orden'    },
    { field: 'tipo'     },
    { field: 'inicial'  },
    { field: 'buscar'   },
    { field: 'base'     },
    { field: 'tabla'    },
    { field: 'maestro'  },
    { field: 'despliega'},
    { field: 'llave'    },
    { field: 'campo'    },
    { field: 'alias'    },
    { field: 'titulo'   },
    { field: 'editarcon'},
    { field: 'ancho'    },
    { field: 'valores'  },
    { field: 'lista'    },
    { field: 'mascara'  },
    { field: 'unico'    },
    { field: 'visible'  },
    { field: 'formula'  },
    { field: 'ordenar'  },
    { field: 'max'      },
  ];

  //declaro mi arrar para enviar los datos a la vista
  rowData: any[];

  get usuario(){
    return this.authentication.usuario;
  }

  
  constructor(private http: HttpClient, private reportsService: ReportsService, 
      private authentication: AuthenticationService, private router: Router) {

  }

  ngOnInit() {
   

  }


  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => {
      if (node.groupData) {
        return { id: node.key, funcion: 'Group' };
      }
      return node.data;
    });
    const selectedDataStringPresentation = selectedData.map(node => `${node.id} ${node.funcion}`).join(', ');

    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

 

  sendFunction(){
    let datos;
    const funcion = this.formulario.funcion;
    const userId = this.formulario.userId;

      this.reportsService.sendFunction(funcion, userId)
      .subscribe(resp =>{
        datos = resp.results
        console.log(datos)
        return this.rowData = datos;
     })
  }

  logOut() {
      this.router.navigateByUrl('/login');
      this.authentication.logOut();
  }

}
