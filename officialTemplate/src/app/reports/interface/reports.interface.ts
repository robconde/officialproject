// To parse this data:
//
//   import { Convert, ShowGridResponse } from "./file";
//
//   const showGridResponse = Convert.toShowGridResponse(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface ShowGridResponse {
    results: Result[];
}

export interface Result {
    id:        number;
    funcion:   Base;
    orden:     number;
    tipo:      Buscar;
    inicial:   string;
    buscar:    Buscar;
    base:      Base;
    tabla:     Base;
    maestro:   string;
    despliega: string;
    llave:     string;
    campo:     string;
    alias:     string;
    titulo:    string;
    editarcon: string;
    ancho:     number;
    valores:   Valores;
    lista:     Lista;
    mascara:   Mascara;
    unico:     string;
    visible:   Visible;
    formula:   null;
    ordenar:   Ordenar;
    max:       number;
}

export enum Base {
    Maedep = "MAEDEP",
}

export enum Buscar {
    C = "C",
    N = "N",
}

export enum Lista {
    Empty = "",
    SiNo = "Si,No",
    The01234 = "0,1,2,3,4",
}

export enum Mascara {
    Empty = "",
    The0 = "#####0",
    The000 = "#####0.00",
}

export enum Ordenar {
    Empty = "",
    N = "N",
}

export enum Valores {
    Empty = "",
    SN = "S,N",
    The01234 = "0,1,2,3,4",
}

export enum Visible {
    N = "N",
    S = "S",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toShowGridResponse(json: string): ShowGridResponse {
        return cast(JSON.parse(json), r("ShowGridResponse"));
    }

    public static showGridResponseToJson(value: ShowGridResponse): string {
        return JSON.stringify(uncast(value, r("ShowGridResponse")), null, 2);
    }
}

function invalidValue(typ: any, val: any, key: any = ''): never {
    if (key) {
        throw Error(`Invalid value for key "${key}". Expected type ${JSON.stringify(typ)} but got ${JSON.stringify(val)}`);
    }
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`, );
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        const map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        const map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any, key: any = ''): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val, key);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        const l = typs.length;
        for (let i = 0; i < l; i++) {
            const typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        const result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps, prop.key);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps, key);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

function m(additional: any) {
    return { props: [], additional };
}

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "ShowGridResponse": o([
        { json: "results", js: "results", typ: a(r("Result")) },
    ], false),
    "Result": o([
        { json: "id", js: "id", typ: 0 },
        { json: "funcion", js: "funcion", typ: r("Base") },
        { json: "orden", js: "orden", typ: 0 },
        { json: "tipo", js: "tipo", typ: r("Buscar") },
        { json: "inicial", js: "inicial", typ: "" },
        { json: "buscar", js: "buscar", typ: r("Buscar") },
        { json: "base", js: "base", typ: r("Base") },
        { json: "tabla", js: "tabla", typ: r("Base") },
        { json: "maestro", js: "maestro", typ: "" },
        { json: "despliega", js: "despliega", typ: "" },
        { json: "llave", js: "llave", typ: "" },
        { json: "campo", js: "campo", typ: "" },
        { json: "alias", js: "alias", typ: "" },
        { json: "titulo", js: "titulo", typ: "" },
        { json: "editarcon", js: "editarcon", typ: "" },
        { json: "ancho", js: "ancho", typ: 0 },
        { json: "valores", js: "valores", typ: r("Valores") },
        { json: "lista", js: "lista", typ: r("Lista") },
        { json: "mascara", js: "mascara", typ: r("Mascara") },
        { json: "unico", js: "unico", typ: "" },
        { json: "visible", js: "visible", typ: r("Visible") },
        { json: "formula", js: "formula", typ: null },
        { json: "ordenar", js: "ordenar", typ: r("Ordenar") },
        { json: "max", js: "max", typ: 0 },
    ], false),
    "Base": [
        "MAEDEP",
    ],
    "Buscar": [
        "C",
        "N",
    ],
    "Lista": [
        "",
        "Si,No",
        "0,1,2,3,4",
    ],
    "Mascara": [
        "",
        "#####0",
        "#####0.00",
    ],
    "Ordenar": [
        "",
        "N",
    ],
    "Valores": [
        "",
        "S,N",
        "0,1,2,3,4",
    ],
    "Visible": [
        "N",
        "S",
    ],
};
