import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports/reports.component';
import { RouterModule } from '@angular/router';
import { ReportsRoute } from './reports.routing';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import 'ag-grid-enterprise';
import { ReportsService } from './services/reports.service';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ReportsComponent],
  exports:[
    ReportsComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    RouterModule.forChild(ReportsRoute),
    FormsModule
  ],
  providers:[ReportsService]
})
export class ReportsModule { }
