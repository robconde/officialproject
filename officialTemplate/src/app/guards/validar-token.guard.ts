import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ValidarTokenGuard implements CanActivate, CanLoad {

  constructor(private authentication: AuthenticationService, private router: Router) { }

  canActivate(): Observable<boolean> | boolean {
  
    return this.authentication.validarToken()
      .pipe(
        tap(valid => {
          if (!valid) {
            this.router.navigateByUrl('/login')
          }
        })
      )
  }
  canLoad(): Observable<boolean> | boolean {
   
    return this.authentication.validarToken()
      .pipe(
        tap(valid => {
          if (!valid) {
            this.router.navigateByUrl('/login')
          }
        })
      )
  }
}
